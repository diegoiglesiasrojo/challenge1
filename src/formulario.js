import React, {Component} from 'react';
import * as firebase from "firebase/app";
import "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import FormularioR from './formularioR';
import FormularioL from './formularioL';

class Formulario extends Component{

    constructor(){
        super();
        this.state = {
            usuario: null
        };
        this.logInG = this.logInG.bind(this);
        this.logOut = this.logOut.bind(this);
        this.renderizarBotones = this.renderizarBotones.bind(this);
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged(usuario =>{
            this.setState({
                usuario: usuario
            })
        })
    }

    logInG(){
        const logG = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(logG)
        .catch(error => alert(error.message))
    }

    logOut(){
        firebase.auth().signOut()
        .catch(error => alert(error.message))
    }

    renderizarBotones(){
        if(this.state.usuario){
            let infoFooter;
            const emailActual = firebase.auth().currentUser.email;
            firebase.firestore().collection("usuarios").doc(emailActual).get()
            .then(function(dato){
                if (dato.exists) {
                    infoFooter = dato.data().ultimoLogin
                    console.log(infoFooter)
                } else{
                    infoFooter = "Bienvenido! Es su primer login en la aplicacion";
                    console.log(infoFooter)
                }
            }).catch(function(error){
                console.log("Error getting document:", error);
            });

            function tiempo(){
                const fecha = new Date();
                const cargarFecha = `Su anterior fecha de logueo fue el ${fecha.getDate()}/${fecha.getMonth()}/${fecha.getFullYear()} a las ${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;
                firebase.firestore().collection("usuarios").doc(emailActual).set({
                    ultimoLogin: cargarFecha,
                })
                .catch(function(error) {
                    console.error("Error writing document: ", error);
                });
                
            };
            setTimeout(tiempo, 3000);

            return(
                <div>
                    <h2>Bienvenido a la app!</h2>
                    <button id="botonLogout" onClick={this.logOut}>Cerrar sesión</button>
                </div>
            );
        } else{
            return(
                <div className="contenedor">
                    <h1>Bienvenido a la app!</h1>
                    <p>Por favor inicia sesión o registrate</p>
                    <div id="contF">
                        <button id="inicioG" onClick={this.logInG}>Iniciar sesión con Google</button>
                        <FormularioL/>
                        <FormularioR/>
                    </div>
                </div>
            );
        }
    }

                /*    <footer>footer{infoFooter}</footer>*/

    render(){
        return(
            <div>
                {this.renderizarBotones()}
            </div>
        );
    }
}

export default Formulario;