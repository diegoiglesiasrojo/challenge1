import React, {Component} from 'react';
import * as firebase from "firebase/app";
import "firebase/app";
import "firebase/auth";
import "firebase/firestore";

class FormularioL extends Component{

    constructor(){
        super();
        this.state = {
            rec: 0
        };
        this.mostrarRecuperacion = this.mostrarRecuperacion.bind(this);
        this.cambiarRec = this.cambiarRec.bind(this);
        this.recuperacion = this.recuperacion.bind(this);
    }

    logIn(e){
        e.persist();
        e.preventDefault();
        const email = e.target.emailL.value;
        const password = e.target.passwordL.value;
        firebase.auth().signInWithEmailAndPassword(email, password).catch((error) => {
            let errorCode = error.code;
            let errorMessage = error.message;
            alert(errorCode, errorMessage);
        });
    }

    cambiarRec(){
        this.setState({
            rec: 1
        })
    }

    mostrarRecuperacion(){
        if(this.state.rec === 1){
            return(
                <div id="formRec">
                    <form name="recuperacion" onSubmit={this.recuperacion}>
                        <input type="email" id="emailRec" name="emailRec" placeholder="Introduce tu email para recuperar tu contraseña" required/>
                        <input id="recInput"type="submit" value="Recuperar"/>
                    </form>
                </div>
            )
        }
    }

    recuperacion(e){
        e.persist();
        e.preventDefault();
        firebase.auth().sendPasswordResetEmail(e.target.emailRec.value)
        .then(alert(`Se ha enviado un correo a ${e.target.emailRec.value}, por favor revise su casilla de mensajes`));
        this.setState({ rec: 0})
    }

    render(){
        return(
            <div id="login">
                <form name="login" onSubmit={this.logIn}>
                    <input type="email" id="emailL" name="emailL" placeholder="Introduce tu email" required/>
                    <input type="password" name="passwordL" id="passwordL" placeholder="Introduce tu contraseña" required/>
                    <input type="submit" value="Iniciar sesión"/>
                    <button type="reset">Borrar</button>
                </form>
                <button id="cambiarRec"onClick={this.cambiarRec}>¿Olvidaste tu contraseña?</button>
                {this.mostrarRecuperacion()}
            </div>
        );
    }
}

export default FormularioL;