import React, {Component} from 'react';
import * as firebase from "firebase/app";
import "firebase/app";
import "firebase/auth";
import "firebase/firestore";

class FormularioR extends Component{

    Register(e){
        e.persist();
        e.preventDefault();
        const email = e.target.emailR.value;
        const password = e.target.passwordR.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).catch((error) => {
            let errorCode = error.code;
            let errorMessage = error.message;
            alert(errorCode, errorMessage);
        });
    }

    render(){
        return(
            <div id="registro">
                <form name="register" onSubmit={this.Register}>
                    <input type="email" id="emailR" name="emailR" placeholder="Introduce tu email" required/>
                    <input type="password" name="passwordR" id="passwordR" placeholder="Introduce tu contraseña" required/>
                    <input id="registroInput" type="submit" value="Registrar"/>
                    <button type="reset">Borrar</button>
                </form>
            </div>
        );
    }
}

export default FormularioR;