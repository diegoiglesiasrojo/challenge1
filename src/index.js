import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyDaSkmQs10nGltHDwKlNvWrKHTUHXw-xzc",
    authDomain: "challenge1-f1f06.firebaseapp.com",
    databaseURL: "https://challenge1-f1f06.firebaseio.com",
    projectId: "challenge1-f1f06",
    storageBucket: "challenge1-f1f06.appspot.com",
    messagingSenderId: "305962622707",
    appId: "1:305962622707:web:821f5219c70d7553142186"
};
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
